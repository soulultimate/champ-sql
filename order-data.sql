select
day(inv.DocDate) as 'SaleDate',month(inv.DocDate) as 'SaleMonth',year(inv.DocDate) as 'SaleYear',
day(so.DocDate) as 'SoDate',month(so.DocDate) as 'SoMonth',year(so.DocDate) as 'SoYear',
sos.DocNo as 'SoDocNo',sovs.Docno as 'SoVoidDocNo','CancleRemark'=(select Name from BCCancelReason where Code = sov.VoidReasonCode),invs.SORefNo,invs.DocNo as 'InvNo',cn.DocNo as 'CNNo','CNRemark'=(select name from bccnmaster where code = cn.CauseCode),cn.MyDescription as 'CNDetail',
sos.ItemCode,'ItemName'=(select Name1 from BCITEM where Code = sos.ItemCode),
sos.Qty as 'SoQty',sovs.Qty as 'SoVoidQty',invs.Qty as 'InvQty',cns.DiscQty as 'CNQty',
sos.UnitCode as 'SoUnit',sovs.UnitCode as 'SoVoidUnit',invs.UnitCode as 'InvUnit',cns.UnitCode as 'CNUnit',
sos.WHCode as 'SoWH',invs.WHCode as 'InvWH',
inv.ArCode,'ARNAME'=(select name1 from BCAR where code = inv.ArCode),
inv.AllocateCode,'AllocateName'=(select Name from Bcallocate where code = inv.AllocateCode),inv.OrderChannel,'OrdercName'=(select name from BCOrderchannel where code =  inv.OrderChannel),
case inv.IsConditionSend
when '0' then 'รับเอง'
when '1' then 'ส่งให้'
end as 'SendCondition',
case inv.BillType
when '0' then 'ขายสินค้าเงินสด'
when '1' then 'ขายสินค้าเงินเชื่อ'
when '2' then 'ขายบริการเงินสด'
when '3' then 'ขายบริการเงินเชื่อ'
end as 'BillType',
sos.SaleCode as 'SoSaleCode','SoSaleName'=(select Name from BCSale where Code =so.SaleCode),
inv.SaleCode as 'InvSaleCode','InvSaleName'=(select Name from BCSale where Code = inv.SaleCode)
from 
BCSaleOrderSub sos  
left join BCSaleOrder so on so.DocNo = sos.DocNo
left join BCARINVOICESUB invs on invs.SORefNo = sos.DocNo and invs.ItemCode = sos.ItemCode
left join BCARINVOICE inv on inv.DocNo = invs.DocNo
left join bcsovoidsub sovs on sovs.SONo = sos.DocNo and sovs.ItemCode = sos.ItemCode
left join bcsovoid sov on sov.Docno = sovs.Docno
left join BCCreditNoteSub cns on cns.InvoiceNo = invs.DocNo and cns.ItemCode = invs.ItemCode
left join BCCreditNote cn on cn.DocNo = cns.DocNo
order by sos.DocNo asc