select 
ar.code as 'ArCode',ar.Name1 as 'ArName',ar.BillAddress,ar.DebtLimitBal as 'CreditBalance',ar.DebtAmount,
ar.GroupCode,arg.Name as 'GroupName',ar.SaleCode,sl.Name as 'SalesName'
from BCAr ar
left join BCARGroup arg on arg.Code = ar.GroupCode
left join BCSale sl on sl.Code = ar.SaleCode