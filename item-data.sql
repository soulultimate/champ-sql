select itm.Code as 'ItemCode',itm.Name1 as 'ItemName',
itm.BrandCode,itb.Name as 'BrandName',itm.GroupCode,itg.Name as 'GroupName',itm.TypeCode,itt.Name as 'TypeName',itm.CategoryCode,itc.Name as 'CategoryName',itm.FormatCode,itf.Name as 'FormatName',
itm.DefStkUnitCode,itm.DefSaleUnitCode,itm.DefBuyUnitCode,
itm.AverageCost,itm.LastBuyPrice,
itm.SCGProductId,itm.SCGProductname,itm.SCGUnitcode,
itm.Weight,itm.StockQty,itm.RemainInQty,itm.RemainOutQty
from BCITEM itm
left join BCItemBrand itb on itb.Code = itm.BrandCode
left join BCItemGroup itg on itg.Code = itm.GroupCode
left join BCItemType itt on itt.Code = itm.TypeCode
left join BCItemCategory itc on itc.Code = itm.CategoryCode
left join BCItemFormat itf on itf.Code = itm.FormatCode